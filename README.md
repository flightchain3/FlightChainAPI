# FlightChain API v3

## Description

This REST API Interface, part of new version of SITA FlightChain(v3) supporting rich search, private data, etc


The API interface exposes the chaincode. This is a CRU (as opposed to a CRUD) interface. There is no delete on a
blockchain so the operations are Create, Read, Update.
 

## Getting Started 

### Installation Pre-requisites

You must have node v9 installed and all the hyperledger fabric requirements installed - see top level project README

Build uses gulp to perform build tasks
```
npm install -g gulp-cli
```

Install npm modules
```
npm install
```

## Register admin/users

For local testing, it is necessary to bootstrap the network with an admin user
and some airline/airport users. The certificates & pem file are stored in `bootstrap/hfc-key-store` by default

> **NOTE** You must run this script every time you rebuild your blockchain network.
This script will create an admin users, and register users for BA, MIA, GVA & LHR.

```
export CA_ENDPOINT=http://localhost:7054
export MSPID=SITAMSP
./setupUsers.sh
```


## Registering users on k8s environment

### Setup the user accounts.

Use the k8s dashboard to exec into one of the `cli` pods. 

    git clone https://gitlab.com/FlightChain3/FlightChainAPI.git
    npm i
    export MSPID=$CORE_PEER_LOCALMSPID
    export CA_ENDPOINT=https://ca-sitaeu.fabric12:7054
    # The FABRIC_CA_SERVER_CA_NAME value is the FABRIC_CA_SERVER_CA_NAME env variable on the CA pod.
    export FABRIC_CA_SERVER_CA_NAME=ca 
    
This script file will create accounts on the Fabric CA server -  FlightChainBA, FlightChainMIA, FlightChainLHR
These files are stored in ./bootstrap/hfc-key-store 

    ./setupUsers.sh


### Configuring the  

    cat bootstrap/hfc-key-store/FlightChainBA

TODO: Update docs with how to transfer this data to the FLightChainAPI CI variables for deployment to the k8s pods.


## Kubectl & Minikube Installation

> ** Note 1 ** Ensure that the VM is using 2 processors instead of 1. 
   Go to Settings -> System -> Processor and change value to 2 CPUs.

> ** Note 2 ** For the following error:  Error: 2 UNKNOWN: access denied: channel [channel-flight-chain]….” ,
Go to FlightChainAPI and run the removeUsers.sh followed by the setupUsers.sh to regenerate user private and public keys

> ** Note 3 ** [External IP]:3004/version - will not work locally as it is dependent on the UI,
which is inaccessible from within the minikube pod.

> ** Note 4 ** In order to run the application locally or on minikube,
go to app.module.ts and uncomment the line "entities: ['src/**/**.entity{.ts,.js}'],".
   


##### Install and Set Up kubectl 
https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux

```bash
$ curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

$ sudo mkdir -p /usr/local/bin/ 
$ sudo install minikube /usr/local/bin/

$ sudo minikube start --vm-driver=none

$ sudo minikube dashboard --url
```


## FlightChain Setup on Minikube

```bash
$ sudo kubectl create namespace flight-chain-api
$ sudo kubectl config set-context --current --namespace=flight-chain-api

# Navigate to the root folder of FlightchainAPI
$ sudo kubectl apply -f minikube-k8s/crypto-volume.yml
$ sudo kubectl apply -f minikube-k8s/crypto-volume-claim.yml

# Navigate to the bootstrap folder in FlightchainAPI and enter the following command:
# If you see the error: "error: unable to read client-key /home/osboxes/.minikube/client.key for minikube", run the command "sudo chmod 777 /home/osboxes/.minikube/client.key".
$ sudo kubectl create configmap fabric-api-cert-config-"fchain-api" --from-file=hfc-key-store -o yaml --dry-run | kubectl apply --namespace="flight-chain-api" -f -

$ docker build -f Dockerfile.flightchainapi-minikube -t flightchainapi .

# Inside the minikube-k8s folder, run the following command: 
$ sudo kubectl apply -f deployment-minikube.yml

# To get the external IP of the pod
$ kubectl get pods -o wide --namespace flight-chain-api

# Install postman inside the VM and run the following command to test your configuration: 
# <External IP>:3004/auth/login
```
 
 
## Running the app (for local network testing)
 > **NOTE** If getting error "No repository for "User" was found." when using 'npm run start' in FlightChainAPI locally  - 
  Check app.module.ts file and check that this line is correct -
  entities: ['src/**/**.entity{.ts,.js}', 'dist/**/**.entity{.ts,.js}'],

When you start the app, you must specify via environment variables which port the instance of the REST API is listening on and what airline or airport identity is related to this instance.

There are some convenience scripts in package.json to set these environment variables. 
Addendum : The Angular Bootstram for Login / Sign in form is made the landing Page.

```bash

# launch in watch mode for SAA
$ npm run start:saa-local


```

## User Interface

View the swagger docs for the API on `http://localhost:<LISTEN_PORT>/docs`


## API Security

API security uses a standard Json Web Token (JWT) approach whereby users must authenticate via the /auth/login method with a user/password combination.
If successful, this will return a JWT in the response. This token must be added to the Authorization header of any subsequent request in the form Bearer <<JWT>>.
Note: the JWT is time limited so client apps should handle the possibility that the token may expire, in which case they will need to re-authenticate to obtain a new token.

## Tests

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

