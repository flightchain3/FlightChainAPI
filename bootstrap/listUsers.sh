#!/usr/bin/env bash

docker exec -e "CA_ENDPOINT=http://localhost:7054" -e "MSPID=SITAMSP" ca.sita.aero fabric-ca-client enroll -u http://admin:Fabric_S1talab@localhost:7054 --tls.certfiles /etc/hyperledger/fabric-ca-server-config/ca.sandbox.sita.aero-cert.pem

docker exec -e "CA_ENDPOINT=http://localhost:7054" -e "MSPID=SITAMSP" ca.sita.aero fabric-ca-client identity list -u http://admin:Fabric_S1talab@localhost:7054 --tls.certfiles /etc/hyperledger/fabric-ca-server-config/ca.sandbox.sita.aero-cert.pem
