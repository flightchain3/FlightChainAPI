#!/usr/bin/env bash

docker exec -e "CA_ENDPOINT=http://localhost:7054" -e "MSPID=SITAMSP" ca.sita.aero fabric-ca-client enroll -u http://admin:Fabric_S1talab@localhost:7054 --tls.certfiles /etc/hyperledger/fabric-ca-server-config/ca.sandbox.sita.aero-cert.pem

docker exec -e "CA_ENDPOINT=http://localhost:7054" -e "MSPID=SITAMSP" ca.sita.aero fabric-ca-client identity remove FlightChainSAA -u http://admin:Fabric_S1talab@localhost:7054 --tls.certfiles /etc/hyperledger/fabric-ca-server-config/ca.sandbox.sita.aero-cert.pem
docker exec -e "CA_ENDPOINT=http://localhost:7054" -e "MSPID=SITAMSP" ca.sita.aero fabric-ca-client identity remove FlightChainMU -u http://admin:Fabric_S1talab@localhost:7054 --tls.certfiles /etc/hyperledger/fabric-ca-server-config/ca.sandbox.sita.aero-cert.pem
docker exec -e "CA_ENDPOINT=http://localhost:7054" -e "MSPID=SITAMSP" ca.sita.aero fabric-ca-client identity remove FlightChainLHR -u http://admin:Fabric_S1talab@localhost:7054 --tls.certfiles /etc/hyperledger/fabric-ca-server-config/ca.sandbox.sita.aero-cert.pem
docker exec -e "CA_ENDPOINT=http://localhost:7054" -e "MSPID=SITAMSP" ca.sita.aero fabric-ca-client identity remove FlightChainMIA -u http://admin:Fabric_S1talab@localhost:7054 --tls.certfiles /etc/hyperledger/fabric-ca-server-config/ca.sandbox.sita.aero-cert.pem
docker exec -e "CA_ENDPOINT=http://localhost:7054" -e "MSPID=SITAMSP" ca.sita.aero fabric-ca-client identity remove FlightChainGVA -u http://admin:Fabric_S1talab@localhost:7054 --tls.certfiles /etc/hyperledger/fabric-ca-server-config/ca.sandbox.sita.aero-cert.pem
docker exec -e "CA_ENDPOINT=http://localhost:7054" -e "MSPID=SITAMSP" ca.sita.aero fabric-ca-client identity remove FlightChainBA -u http://admin:Fabric_S1talab@localhost:7054 --tls.certfiles /etc/hyperledger/fabric-ca-server-config/ca.sandbox.sita.aero-cert.pem
