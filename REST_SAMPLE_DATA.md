### REST APIs <a name="rest-apis"></a>

Below are some sample json requests that can be used with the REST Swagger Documentation (http://swagger.flightchain.aero). 


### `POST` Create a new flight on the network

This API call shows an example of POSTing a new flight to the ledger. This will return a unique flight key for this flight (`2017-04-05MIAAA1481`).   

```
curl -X POST 'https://flightchain.blockchainsandbox.aero/' -d "
{
  "operatingAirline": {
    "iataCode": "AA",
    "icaoCode": "AAL",
    "name": "American Airlines"
  },
  "aircraftType": {
    "icaoCode": "B757",
    "modelName": "757",
    "registration": "N606AA"
  },
  "flightNumber": {
    "airlineCode": "AA",
    "trackNumber": "1481"
  },
  "departureAirport": "MIA",
  "arrivalAirport": "SDQ",
  "originDate": "2017-04-05",
  "departure": {
    "scheduled": "2017-04-05T12:27:00-04:00",
    "estimated": "2017-04-05T12:27:00-04:00",
    "terminal": "N",
    "gate": "D47"
  },
  "arrival": {
    "scheduled": "2017-04-05T14:38:00-04:00",
    "terminal": "",
    "gate": "",
    "baggageClaim": {
      "carousel": ""
    }
  },
  "flightStatus": "Scheduled"
}
"
```

### `PATCH` Update an existing flight on the network

This API call shows an example of POSTing an update to an existing flight (in this case the estimated departure time is updated). 
The flight is updated by the flight key parameter. For the sample json above, the flight key is `2017-04-05MIAAA1481`. 

```
url -X PATCH 'https://flightchain.blockchainsandbox.aero/2017-04-05MIAAA1481' -d "
{
  "departure": {
    "estimated": "2017-04-05T12:34:00-04:00",
  }
}
"
```

#### `GET` Retrieve live status on a flight on the network


This API call shows an example of querying the realtime status of a flight.

```
curl -X GET 'https://flightchain.blockchainsandbox.aero/2017-04-05MIAAA1481'
```

#### `GET` Retrieve update history for a flight

This API call shows an example of retrieving the history of all updates to a flight. This will return the audit trail of
the updates made, including who made the updates.

```
curl -X GET 'https://flightchain.blockchainsandbox.aero/2017-04-05MIAAA1481/history'
```

### ACI ACRIS  <a name="aci-acris"></a>
There are many existing data standards for representing flight status data. Flight Chain uses [ACRIS](http://www.aci.aero/About-ACI/Priorities/Airport-IT/ACRIS) 
(a standard defined by [ACI](http://www.aci.aero)). This is a compact & modern data format using JSON with a well defined schema.
