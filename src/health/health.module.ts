import { Module } from '@nestjs/common';
import { FlightChain2Module } from '../flight-chain2/flight-chain.module';
import { HealthController } from './health.controller';

@Module({
    controllers: [HealthController],
    imports: [FlightChain2Module],
})
export class HealthModule {
}
