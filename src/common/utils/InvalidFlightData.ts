import {AcrisFlight} from '../../acris-schema/AcrisFlight';

export class InvalidFlightData implements Error {
    public message: string;
    public name: string;
    private flight: AcrisFlight;

    constructor(msg: string, flight: AcrisFlight) {
        this.message = msg;
    }
}
