export class UniqueKeyException implements Error {
    public name: string;
    public message: string;

    constructor(e: Error) {
        this.message = e.message;
        this.name = e.name;
    }
}
