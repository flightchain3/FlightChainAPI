import { createLogger, format, Logger, transports } from 'winston';
import {LoggerHelper} from './LoggerHelper';

export class Loggers {
    public static awssqs: Logger = LoggerHelper.getLogger('SQS');

    public static config: Logger = LoggerHelper.getLogger('CONFIG');

    public static grpc: Logger = LoggerHelper.getLogger('GRPC');

    public static hlf: Logger = LoggerHelper.getLogger('FABRIC');

    public static pusher: Logger = LoggerHelper.getLogger('PUSHER');
}
