import {createLogger, format, transports} from 'winston';

export class LoggerHelper {
    public static getLogger = (label: string) => {
        return createLogger({
            exitOnError: false,
            format: format.combine(
                format.colorize(),
                format.splat(),
                format.simple(),
                format.label({ label })),
            transports: [new transports.Console({
                level: 'debug',
            })],
        });
    }
}
