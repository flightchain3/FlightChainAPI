export interface IConfigOptions {
    hlf: IHlfConfigOptions;
}

export interface IAdminCreds {
    MspID: string;
}

export interface ITLSOptions {
    trustedRoots: any[];
    verify: boolean;
}

export interface IHlfConfigOptions {
    walletPath: string;
    admin: IAdminCreds;
    channelId: string;
    chaincodeId: string;
    peerUrls: string[];
    eventUrl: string;
    ordererUrl: string;
    tlsOptions: ITLSOptions;
    caName: string;
}
