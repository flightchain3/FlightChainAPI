import { IConfigOptions } from './config.model';
import { EnvConfig } from './env';

export const Appconfig: IConfigOptions = {
    hlf: {
        admin: {
            MspID: EnvConfig.MSPID,
        },
        caName: 'ca.sita.aero',
        chaincodeId: 'flightchain',
        channelId: EnvConfig.CHANNEL,
        tlsOptions: {
            trustedRoots: [],
            verify: false,
        },
        walletPath: EnvConfig.HFC_STORE_PATH,
    },
} as IConfigOptions;
