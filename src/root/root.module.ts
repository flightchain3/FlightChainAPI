import { Module } from '@nestjs/common';
import { RootController } from './root.controller';

@Module({
    controllers: [RootController],
    imports: [],
    providers: [] })
export class RootModule {
}
