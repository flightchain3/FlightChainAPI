import {Controller, Get, Res} from '@nestjs/common';

/**
 */
@Controller('')
export class RootController {

    /**
     * TODO: Is there a better way to do the redirect to the swagger docs from the root URL to /docs ?
     * @param response
     */
     @Get('')
     public async getRootRedirectToDocs(@Res() response) {
        return response.redirect('docs');
    }
}
