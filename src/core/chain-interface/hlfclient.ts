import { Injectable } from '@nestjs/common';
import FabricClient = require('fabric-client');
import {
    IKeyValueStore, ProposalErrorResponse, ProposalResponse,
    ProposalResponseObject, TransactionRequest,
} from 'fabric-client';
import * as Client from 'fabric-client';
import * as fs from 'fs';

import { Appconfig } from '../../common/config/appconfig';
import { EnvConfig } from '../../common/config/env';
import { Log } from '../../common/utils/logging/log.service';
import { FlightChainMethod } from '../../flight-chain2/flight-chain2.service';

import { ChainService } from './chain.service';
import { HlfConfig } from './hlfconfig';
import { HlfInfo } from './logging.enum';

@Injectable()
export class HlfClient extends ChainService {

    /**
     * The config will be initialised in the constructor - based on the target environemnt - local 'sita_basic_network',
     * or the k8s environmnet in AWS
     */
    private static config: any = {};

    /**
     * This is the configuration to use on the k8s environment in AWS.
     */
    private static configKubernetesEnvironment = {
        orderers: [
            {
                endpoint: 'grpcs://orderer1st-sita.fchain:7050',
                pemFile: '/tmp/crypto/ordererOrganizations/fchain/orderers/'
                    + 'orderer1st-sita.fchain/msp/tlscacerts/tlsca.fchain-cert.pem',
            },
            {
                endpoint: 'grpcs://orderer2nd-sita.fchain:7050',
                pemFile: '/tmp/crypto/ordererOrganizations/fchain/orderers/'
                    + 'orderer2nd-sita.fchain/msp/tlscacerts/tlsca.fchain-cert.pem',
            },
        ],
        peers: [
            {
                endpoint: 'grpcs://peer1st-fchaineuro.fchain:7051',
                pemFile: '/tmp/crypto/peerOrganizations/fchaineuro.fchain/peers/'
                    + 'peer1st-fchaineuro.fchain/msp/tlscacerts/tlsca.fchaineuro.fchain-cert.pem',
            },
            {
                endpoint: 'grpcs://peer2nd-fchaineuro.fchain:7051',
                pemFile: '/tmp/crypto/peerOrganizations/fchaineuro.fchain/peers'
                    + '/peer2nd-fchaineuro.fchain/msp/tlscacerts/tlsca.fchaineuro.fchain-cert.pem',
            },
        ],
    };

    /**
     * This is the local testing environment.
     */
    private static configLocalTestNetwork = {
        orderers: [
            {
                endpoint: 'grpc://localhost:7050',
                pemFile: '../flightchainsmartcontract/sita-basic-network/crypto-config/ordererOrganizations/'
                    + 'sita.aero/orderers/orderer.sita.aero/msp/tlscacerts/tlsca.sita.aero-cert.pem',
            },
        ],
        peers: [
            {
                endpoint: 'grpc://localhost:7051',
                pemFile: '../flightchainsmartcontract/sita-basic-network/crypto-config/peerOrganizations/'
                    + 'sandbox.sita.aero/peers/peer0.sandbox.sita.aero/msp/tlscacerts/tlsca.sandbox.sita.aero-cert.pem',
            },
            {
                endpoint: 'grpc://localhost:8051',
                pemFile: '../flightchainsmartcontract/sita-basic-network/crypto-config/peerOrganizations/'
                    + 'sandbox.sita.aero/peers/peer1.sandbox.sita.aero/msp/tlscacerts/tlsca.sandbox.sita.aero-cert.pem',
            },
        ],
    };

    /**
     * This is the local-k8 testing environment.
     */
    private static configLocalK8TestNetwork = {
        orderers: [
            {
                endpoint: 'grpc://10.0.2.15:7050',
                pemFile: '/tmp/crypto/ordererOrganizations/'
                    + 'sita.aero/orderers/orderer.sita.aero/msp/tlscacerts/tlsca.sita.aero-cert.pem',
            },
        ],
        peers: [
            {
                endpoint: 'grpc://10.0.2.15:7051',
                pemFile: '/tmp/crypto/peerOrganizations/'
                    + 'sandbox.sita.aero/peers/peer0.sandbox.sita.aero/msp/tlscacerts/tlsca.sandbox.sita.aero-cert.pem',
            },
            {
                endpoint: 'grpc://10.0.2.15:8051',
                pemFile: '/tmp/crypto/peerOrganizations/'
                    + 'sandbox.sita.aero/peers/peer1.sandbox.sita.aero/msp/tlscacerts/tlsca.sandbox.sita.aero-cert.pem',
            },
        ],
    };

    constructor(public hlfConfig: HlfConfig) {
        super(hlfConfig);

        if (EnvConfig.USE_SITA_TEST_FABRIC_NETWORK) {
            console.log('***************************************');
            console.log('Using the local testing fabric network.');
            console.log('Ensure you have FlightChainSmartContract and have run deployChainCode.sh');
            console.log('Using the local testing fabric network.');
            console.log('***************************************');
            HlfClient.config = HlfClient.configLocalTestNetwork;

        } else if (EnvConfig.FABRIC_NETWORK === 'k8-local') {
            console.log('Using the k8s test environment.');
            HlfClient.config = HlfClient.configLocalK8TestNetwork;
        } else {
            console.log('Using the k8s fabric environment.');
            HlfClient.config = HlfClient.configKubernetesEnvironment;
        }

        console.log('Peer&Orderer config: ', HlfClient.config);
    }

    /**
     * init
     * @returns {Promise<any>}
     * @memberof ChainService
     */
    public init(): Promise<any> {

        this.hlfConfig.options = Appconfig.hlf;
        this.hlfConfig.client = new FabricClient();

        Log.hlf.info('HlfClient.init()', this.hlfConfig);

        return FabricClient
            .newDefaultKeyValueStore({
                path: this.hlfConfig.options.walletPath,
            })
            .then((wallet: IKeyValueStore) => {
                console.log(wallet);
                // assign the store to the fabric client
                this.hlfConfig.client.setStateStore(wallet);
                const cryptoSuite = FabricClient.newCryptoSuite();
                // use the same location for the state store (where the users' certificate are kept)
                // and the crypto store (where the users' keys are kept)
                const cryptoStore = FabricClient.newCryptoKeyStore({ path: this.hlfConfig.options.walletPath });
                cryptoSuite.setCryptoKeyStore(cryptoStore);
                this.hlfConfig.client.setCryptoSuite(cryptoSuite);
                this.hlfConfig.channel = this.hlfConfig.client.newChannel(this.hlfConfig.options.channelId);

                HlfClient.config.peers.forEach((peer) => {

                    // tslint:disable-next-line: no-shadowed-variable
                    const connectionOptions: Client.ConnectionOpts = {};
                    // tslint:disable-next-line: no-shadowed-variable
                    const data = fs.readFileSync(peer.pemFile);
                    connectionOptions.pem = Buffer.from(data).toString();
                    Log.hlf.debug(`Connecting to peer ${peer.endpoint}`, connectionOptions);
                    console.log('connectionOptions', connectionOptions);
                    const peerObj = this.hlfConfig.client.newPeer(peer.endpoint, connectionOptions);
                    this.hlfConfig.channel.addPeer(peerObj, EnvConfig.MSPID);
                    this.hlfConfig.targets.push(peerObj);

                    if (!this.hlfConfig.eventHub) {
                        console.log('Initialising the event hub');
                        this.hlfConfig.eventHub = this.hlfConfig.channel.newChannelEventHub(peerObj);
                    }
                });

                const connectionOptions: Client.ConnectionOpts = {};
                const data = fs.readFileSync(HlfClient.config.orderers[0].pemFile);
                connectionOptions.pem = Buffer.from(data).toString();
                Log.hlf.debug(`Connecting to orderer ${HlfClient.config.orderers[0].endpoint}`, connectionOptions);
                this.hlfConfig.channel.addOrderer(this.hlfConfig.client.newOrderer(
                    HlfClient.config.orderers[0].endpoint, connectionOptions));
                Log.hlf.info(HlfInfo.INIT_SUCCESS);
            });
    }

    /**
     * Query hlf
     *
     * @param {FlightChainMethod} chainMethod
     * @param {string[]} params
     * @param transientMap
     * @returns {Promise<any>}
     * @memberof HlfClient
     */
    public query(chainMethod: FlightChainMethod, params: string[], transientMap?: object): Promise<any> {
        Log.hlf.info(HlfInfo.MAKE_QUERY, chainMethod, params);
        return this.newQuery(chainMethod, params, this.hlfConfig.options.chaincodeId, transientMap)
            .then((queryResponses: Buffer[]) => {
                return Promise.resolve(this.getQueryResponse(queryResponses));
            });
    }

    /**
     * Query the transaction Id
     *
     * @param transactionId
     * @returns {Promise<any>}
     * @memberof HlfClient
     */
    public queryTransaction(transactionId: string): Promise<any> {
        Log.hlf.info(HlfInfo.QUERY_TRANSACTIONID, transactionId);
        return this.newTransactionQuery(transactionId);
    }

    /**
     * invoke
     *
     * @param {FlightChainMethod} chainMethod
     * @param { string[]} params
     * @param transientMap
     * @returns
     * @memberof ChainService
     */
    public invoke(chainMethod: FlightChainMethod, params: string[], transientMap?: object): Promise<any> {
        Log.hlf.info(chainMethod, params);
        return this.sendTransactionProposal(chainMethod, params, this.hlfConfig.options.chaincodeId, transientMap)
            .then((result: { txHash: string; buffer: ProposalResponseObject }) => {
                Log.hlf.info(HlfInfo.CHECK_TRANSACTION_PROPOSAL);
                if (this.isProposalGood(result.buffer)) {
                    // console.log('isProposalGood', result.buffer);
                    this.logSuccessfulProposalResponse(result.buffer);
                    const request: TransactionRequest = {
                        proposal: result.buffer[1],
                        proposalResponses: result.buffer[0] as ProposalResponse[],
                    };
                    Log.hlf.info(HlfInfo.REGISTERING_TRANSACTION_EVENT);

                    const sendPromise = this.hlfConfig.channel.sendTransaction(request);
                    const txPromise = this.registerTxEvent(result.txHash);

                    return Promise.all([sendPromise, txPromise]);
                } else {

                    /**
                     * Handle when one or more of the proposal responses are not valid.
                     * Return as error msg, the first invalid proposal response.
                     */

                    const proposalResponse: ProposalErrorResponse[] = result.buffer[0] as ProposalErrorResponse[];

                    let rejectionMessage: any = null;
                    proposalResponse.forEach((r: any) => {
                        // console.log(r.toString())
                        // console.log(r.status)
                        // console.log(JSON.stringify(r, null, 4))

                        // The object returned (r) doeesn't match properly the Client.ProposalResponse data structure
                        // so we pass it as a <any> data type.
                        // And to extract the actual error message the object needs to be converted to a string.
                        //
                        if (rejectionMessage === null && r.status === 500) {
                            let message = r.toString();
                            console.log(message);
                            if (message.indexOf(' transaction returned with failure: ') !== -1) {
                                message = message.split(' transaction returned with failure: ')[1];

                                try {
                                    rejectionMessage = JSON.parse(message);
                                } catch (e) {
                                    Log.hlf.error(e);
                                }
                            }
                        }
                    });

                    if (rejectionMessage) {
                        return Promise.reject(rejectionMessage);
                    } else {

                        console.log('Unable to extract a valid error message from Client.ProposalResponse'
                            + '-generating generic error');
                        rejectionMessage = {};
                        rejectionMessage.name = 'error';
                        rejectionMessage.status = 500;
                        rejectionMessage.message = 'Unknown Error';
                        return Promise.reject({ name: 'Unknown Error' });
                    }
                }
            })
            .then((results) => {
                if (!results || (results && results[0] && results[0].status !== 'SUCCESS')) {
                    Log.hlf.error('Failed to order the transaction. Error code: ' + results[0].status);
                }

                if (!results || (results && results[1] && results[1].event_status !== 'VALID')) {
                    Log.hlf.error('Transaction failed to be committed to the ledger due to ::'
                        + results[1].event_status);
                }
            });
    }
}
