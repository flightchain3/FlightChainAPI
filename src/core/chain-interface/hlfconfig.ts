import { Injectable } from '@nestjs/common';
import {Channel, ChannelEventHub, Peer} from 'fabric-client';
import Client = require('fabric-client');
import { IHlfConfigOptions } from '../../common/config/config.model';

@Injectable()
export class HlfConfig {
    public options: IHlfConfigOptions;
    public client: Client;
    public channel: Channel;
    public targets: Peer[] = [];
    public eventHub: ChannelEventHub;
}
