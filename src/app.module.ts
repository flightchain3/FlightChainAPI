import { MorganMiddleware } from '@nest-middlewares/morgan';
import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { EnvConfig } from './common/config/env';
import { CoreModule } from './core/core.module';
import { FlightChain2Module } from './flight-chain2/flight-chain.module';
import { HealthModule } from './health/health.module';
import { FrontendMiddleware } from './middleware/FrontendMiddleware';
import { RootModule } from './root/root.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [TypeOrmModule.forRoot(AppModule.getDBSettings()),
      AuthModule, CoreModule, FlightChain2Module, HealthModule, RootModule, UserModule],
})
export class AppModule {

    private static getDBSettings(): any {
        console.log('getting db settings');
        console.log('connecting to db at "' + EnvConfig.USER_DB_HOST + '"');
        return {
            database: EnvConfig.USER_DB_NAME,
          //  entities: ['src/**/**.entity{.ts,.js}'],    //Uncomment this line for minikube/local use
            entities: ['dist/**/**.entity{.ts,.js}'],
            host: EnvConfig.USER_DB_HOST,
            password: EnvConfig.USER_DB_PASSWORD,
            port: 3306,
            synchronize: true,
            type: 'mysql',
            username: EnvConfig.USER_DB_USERNAME,
        };
    }

    constructor() {
        EnvConfig.initialise();
    }

    private configure(consumer: MiddlewareConsumer): void {

        // IMPORTANT! Call Middleware.configure BEFORE using it for routes
        MorganMiddleware.configure( 'combined');
        consumer.apply(MorganMiddleware).forRoutes( {
            // @ts-ignore
            method: RequestMethod.ALL, path: '/flightChain/*' },
        {
          // @ts-ignore
          method: RequestMethod.ALL, path: '/auth/*' });

        consumer.apply(FrontendMiddleware)
            .forRoutes({
                // @ts-ignore
                method: RequestMethod.ALL, path: '*'},
        );
    }
}
