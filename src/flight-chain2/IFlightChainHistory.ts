import {FlightChainData, IFabricTimestamp} from './FlightChainData';

/**
 * Represents the object returned from the API to get the flight history.
 */
export class IFlightChainHistory {
    /**
     * The ACRIS flight data
     */
    public flightData: FlightChainData;
    public isDelete: boolean;
    public txId: string;
    public timestamp: IFabricTimestamp;
}
