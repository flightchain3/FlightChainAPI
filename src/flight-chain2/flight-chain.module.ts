import { Module, NestModule } from '@nestjs/common';
import { MiddlewareConsumer } from '@nestjs/common/interfaces/middleware';
import { ChainModule } from '../core/chain-interface/chain.module';
// import { JwtauthenticationMiddleware } from '../common/middleware/jwtauthentication.middleware';
// import { HlfcredsgeneratorMiddleware } from '../common/middleware/hlfcredsgenerator.middleware';
import { FlightChain2Controller } from './flight-chain2.controller';
import { FlightChain2Service } from './flight-chain2.service';
// import {HlfcredsgeneratorMiddleware} from "../common/middleware/hlfcredsgenerator.middleware";

@Module({
    controllers: [
        FlightChain2Controller,
    ],
    exports: [
        FlightChain2Service,
    ],
    imports: [
        ChainModule,
    ],
    providers: [
        FlightChain2Service,
    ],
})
export class FlightChain2Module implements NestModule {
    public configure(consumer: MiddlewareConsumer): void {

        // if (!EnvConfig.SKIP_MIDDLEWARE) {
        //      consumer
                 // .apply(JwtauthenticationMiddleware, HlfcredsgeneratorMiddleware)
                 // .apply(HlfcredsgeneratorMiddleware)
                 // .forRoutes(FlightChain2Controller);
        // }
    }
}
