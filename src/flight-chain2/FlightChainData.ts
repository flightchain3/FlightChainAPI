/**
 * This is the data model stored on the blockchain.
 * Each ACRIS flight entry/update is stored with the transaction Id and
 * updater Id so it can conveniently be accessed by the client apps.
 */
import {ApiModelProperty} from '@nestjs/swagger';
import {AcrisFlight} from '../acris-schema/AcrisFlight';

export class FlightChainData {
    @ApiModelProperty({required: true})
    public flightData: AcrisFlight;
    @ApiModelProperty({description: 'Unique identifier for the flight,made up of\
     the departure date, depature airport, operating airline and flight number\
      (e.g. 2018-09-13LHRBA0227)',
    required: true})
    public flightKey: string;

    // Which IATA entity updated the data.
    @ApiModelProperty({description: 'IATA code for organisation that updated the data.',
    required: true})
    public updaterId: string;
    @ApiModelProperty({description: 'Blockchain transaction identifier',
    required: true})
    public txId: string;
    // TODO: Check if this docType needs to be set for couchDB. It is in the fabric-samples
    public docType: string;
    // The timestamp is only used
    @ApiModelProperty({description: 'Timestamp of when update was made. This is\
     only populated when getting flight history.',
    required: true})
    public timestamp: Date;
}

export interface IFabricTimestamp {
    seconds: IFabricTimestampSeconds;
}

export interface IFabricTimestampSeconds {
    low: number;
    high: number;
    unsigned: boolean;
}
