import { Body, Controller, Get, HttpCode, HttpException, HttpStatus,
    NotFoundException, Param, Patch, Post, Put, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiImplicitParam, ApiOperation, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { AcrisFlight } from '../acris-schema/AcrisFlight';
import { ACRISFlightValidation } from '../common/utils/ACRISValidation';
import { FLIGHTCHAIN_ROUTE_PREFIX } from '../middleware/FrontendMiddleware';
import { FlightChain2Service } from './flight-chain2.service';
import { FlightChainData } from './FlightChainData';

@ApiUseTags('FlightChain2')
@UseGuards(AuthGuard('jwt'))
@Controller(FLIGHTCHAIN_ROUTE_PREFIX)
// @ApiBearerAuth()
// @UseGuards(AuthGuard('bearer')) - TODO  Authentication disabled - uncomment these 2 lines to reenable
export class FlightChain2Controller {

    constructor(private flightChainService: FlightChain2Service) {
    }

    /**
     * @param channelName
     */
    @ApiOperation({
        description: 'Returns the version of the chaincode deployed',
        title: 'Get chaincode version',
    })

    @Get('/version')
    @ApiResponse({status: 200, description: 'Returns the version of the chaincode.', type: String})
    @ApiResponse({status: 404, description: 'Not flight matching the given flightKey has been found.'})
    public async getVersion(
        @Req() request): Promise<string> {
        return this.flightChainService.getVersion();
    }

    /**
     * Get live flight data from the blockchain
     * @param flightKey
     * @param channelName
     */
    @ApiOperation({
        description: 'Returns the live state of the flight identified by flightKey',
        title: 'Get one flight',
    })
    @ApiImplicitParam({
        description: 'Unique key for each flight. The key is made up of\
         [DepDate][DepAirport][OperatingAirline][OperatingFlightNum]. e.g. 2018-07-22LGWBA0227',
        name: 'flightKey',
        required: true,
        type: 'string',
    })

    @Get('/:flightKey')
    @ApiResponse({description: 'The flight has been successfully returned.', status: 200,
    type: FlightChainData})
    @ApiResponse({description: 'Not flight matching the given flightKey has been found.', status: 404,
    type: NotFoundException})
    public async getOneFlight(
        @Param('flightKey') flightKey): Promise<FlightChainData> {
        return this.flightChainService.getFlight(flightKey);
    }

    /**
     *
     * Get the list of flights match the query.
     *
     * @param query
     */
    @ApiOperation({
        description: 'Returns the list of flights matching supplied query' ,
        title: 'Get flights matching query'})

    @Post('/find')
    @HttpCode(HttpStatus.OK)
    @ApiResponse({description: 'The list of flights have been successfully returned.', isArray: true,
    status: 200, type: FlightChainData})
    @ApiResponse({status: 404, description: 'No flight matching the query has been found.'})
    public async getFlightListWithQuery(
        @Body() query: any): Promise<FlightChainData[]> {
        return this.flightChainService.getFlightListWithQuery(query);
    }

    /**
     * Create a new flight on the blockchain
     * @param flight
     * @param channelName
     */

    @ApiOperation({deprecated: true,
        description: 'When creating a flight with the POST command, the following fields\
         are mandatory: originDate, departureAirport, arrivalAirport,\
          operatingAirline.iataCode and flightNumber.trackNumber',
        title: 'Create a new flight on the network -\
     DEPRECATED, use PUT verb instead'})
    @Post()
    @ApiResponse({description: 'The flight has been successfully created.', isArray: false,
    status: 200, type: FlightChainData})
    @ApiResponse({status: 404, description: 'Not flight matching the given flightKey has been found.'})
    @ApiResponse({status: 400, description: 'The flight already exists, or the input flight data is invalid'})
    public async createFlight(
        @Body() flight: AcrisFlight): Promise<FlightChainData> {

        try {
            ACRISFlightValidation.verifyValidACRIS(flight);
        } catch (e) {
            throw new HttpException('Invalid flight data. Did you pass the mandatory fields\
             (originDate, departureAirport, arrivalAirport, operatingAirline.iataCode\
                 and flightNumber.trackNumber) : ' + e.message, HttpStatus.BAD_REQUEST);
        }

        const flightCreated: FlightChainData = await this.flightChainService.createFlight(flight);
        if (flightCreated === undefined) {
            throw new HttpException('Invalid flight', HttpStatus.BAD_REQUEST);
        }
        return flightCreated;
    }

    /**
     * Create/update a flight on the blockchain
     * @param flight
     * @param channelName
     */
    @ApiOperation({description: 'When using the PUT verb, it is necessary to always\
     pass the mandatory fields: originDate, departureAirport, arrivalAirport,\
      operatingAirline.iataCode and flightNumber.trackNumber',
        title: 'Create a new flight, or update an existing flight on the network.'})
    @Put()
    @ApiResponse({description: 'The flight has been successfully created.', isArray: false,
     status: 200, type: FlightChainData})
    @ApiResponse({status: 400, description: 'The input flight data is invalid'})
    public async createFlightIndempotent(
        @Body() flight: AcrisFlight): Promise<FlightChainData> {
        let key = null;

        try {
            ACRISFlightValidation.verifyValidACRIS(flight);
            key = ACRISFlightValidation.generateUniqueKey(flight);
        } catch (e) {
            throw new HttpException('Invalid flight data. Did you pass the mandatory fields\
             (originDate, departureAirport, arrivalAirport, operatingAirline.iataCode\
                 and flightNumber.trackNumber) : ' + e.message, HttpStatus.BAD_REQUEST);
        }

        const flightCreated: FlightChainData = await this.flightChainService.createFlight(flight);
        if (flightCreated === undefined) {
            throw new HttpException('Invalid flight', HttpStatus.BAD_REQUEST);
        }
        return flightCreated;
    }

    /**
     * Update an existing flight on the network.
     * @param flightKey
     * @param flight
     * @param channelName
     */
    @ApiOperation({title: 'Update an existing flight on the network'})
    @ApiImplicitParam({
        description: 'Unique key for each flight. The key is made up of\
         [DepDate][DepAirport][OperatingAirline][OperatingFlightNum].\
          e.g. 2018-07-22LGWBA0227',
        name: 'flightKey',
        required: true,
        type: 'string',
    })

    @Patch('/:flightKey')
    @ApiResponse({description: 'The flight has been successfully updated.',  isArray: false,
    status: 200, type: FlightChainData})
    @ApiResponse({description: 'Not flight matching the given flightKey has been found.', status: 404})
    @ApiResponse({description: 'The input flight data is invalid', status: 400})
    public async updateFlight(
        @Param('flightKey') flightKey, @Body() flight: AcrisFlight): Promise<FlightChainData> {
        // winstonLogger.debug(`FlightChainController.updateFlight(channelName=${channelName},
        // flightKey=${flightKey})`);
        return this.flightChainService.updateFlight(flightKey, flight);
    }

    /**
     *
     * Get the history of all updates to a flight from the blockchain.
     *
     * @param flightKey
     * @param channelName
     */
    @ApiOperation({
        description: 'Returns the history of udpates for the flight identified by flightKey',
        title: 'Get flight history',
    })
    @ApiImplicitParam({
        description: 'Unique key for each flight. The key is made up of\
         [DepDate][DepAirport][OperatingAirline][OperatingFlightNum].\
          e.g. 2018-07-22LGWBA0227',
        name: 'flightKey',
        required: true,
        type: 'string',
        })

    @Get('/:flightKey/history')
    @ApiResponse({description: 'The flight has been successfully returned.', isArray: true,
    status: 200, type: FlightChainData})
    @ApiResponse({description: 'Not flight matching the given flightKey has been found.', status: 404})
    public async getFlightHistory(
        @Param('flightKey') flightKey): Promise<FlightChainData[]> {
        return this.flightChainService.getFlightHistory(flightKey);
    }

    @ApiOperation({title: 'Get transaction info', description: 'Returns the details of a given transaction'})
    @ApiImplicitParam({
        description: 'Transaction Id returned after every flight creation or update.',
        name: 'transactionId',
        required: true,
        type: 'string',
    })
    @Get('/transaction/:transactionId')
    @ApiResponse({status: 200, description: 'The transaction info has been successfully returned.'})
    @ApiResponse({status: 404, description: 'Not transaction info matching the given transactionId has been found.'})
    @ApiResponse({status: 500, description: 'Unknown internal server error.'})
    public async getTransactionInfo(
        @Param('transactionId') transactionId): Promise<any> {
        // winstonLogger.debug(`FlightChainController.getTransactionInfo(channelName=${channelName},
        // transactionId=${transactionId})`);
        return this.flightChainService.getTransactionInfo(transactionId);
    }
}
