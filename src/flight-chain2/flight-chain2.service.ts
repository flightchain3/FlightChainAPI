import {HttpException, HttpStatus, Injectable, InternalServerErrorException, NotFoundException} from '@nestjs/common';
import _ = require('lodash');

import {AcrisFlight} from '../acris-schema/AcrisFlight';
import {ACRISFlightValidation} from '../common/utils/ACRISValidation';
import {Log} from '../common/utils/logging/log.service';
import {RequestHelper} from '../core/chain-interface/requesthelper';
import {FlightChainData, IFabricTimestamp } from './FlightChainData';
import {IFlightChainHistory} from './IFlightChainHistory';

/**
 * These are the names of the functions in the chaincode.
 */
export enum FlightChainMethod {
    'getVersion' = 'getVersion',
    'getFlight' = 'getFlight',
    'getFlightListWithQuery' = 'getFlightQuery',
    'getFlightHistory' = 'getFlightHistory',
    'createFlight' = 'createFlight',
    'updateFlight' = 'updateFlight',
}

@Injectable()
export class FlightChain2Service {

    private static dateFromBlockTimestamp(timestamp: IFabricTimestamp): Date {
        // Log.hlf.debug('the timestamp is: ' + JSON.stringify(timestamp));
        if (timestamp) {
            return new Date(timestamp.seconds.low * 1000);
        }
        return null;
    }

    /**
     * Creates an instance of CarService.
     * @param {RequestHelper} requestHelper
     * @memberof FlightChain2Service
     */
    constructor(private requestHelper: RequestHelper) {
    }

    /**
     *
     * @returns {Promise<any>}
     * @memberof FlightChain2Service
     */
    public getVersion(): Promise<any> {
        return this.requestHelper.queryRequest(FlightChainMethod.getVersion, []);
    }

    /**
     *
     * @returns {Promise<FlightChainData>}
     * @memberof FlightChain2Service
     */
    public getFlight(flightKey: string): Promise<FlightChainData> {
        return this.requestHelper.queryRequest(FlightChainMethod.getFlight, [flightKey]).then(
            (flight) => {
                if (!flight) {
                    throw new NotFoundException(`Flight does not exist with flightKey: ${flightKey}`);
                }
                return flight;
            },
            (error) => {
                throw new InternalServerErrorException(error);
            },
        );
    }

    /**
     *
     * @returns {Promise<FlightChainData[]>}
     * @memberof FlightChain2Service
     */
    public getFlightListWithQuery(query: any): Promise<FlightChainData[]> {
        return this.requestHelper.queryRequest(FlightChainMethod.getFlightListWithQuery, [JSON.stringify(query)]).then(
            (flights) => {
                if (!flights) {
                    throw new NotFoundException(`no flights exist match the query: ${query}`);
                }
                return flights;
            },
            (error) => {
                throw new InternalServerErrorException(error);
            },
        );
    }

    /**
     *
     * @returns {Promise<FlightChainData[]>}
     * @memberof FlightChain2Service
     */
    public getFlightHistory(flightKey: string): Promise<FlightChainData[]> {
        return this.requestHelper.queryRequest(FlightChainMethod.getFlightHistory, [flightKey]).then(
            (flightHistory) => {
                if (!flightHistory || flightHistory.length === 0) {
                    throw new NotFoundException(`Flight does not exist with flightKey: ${flightKey}`);
                }
                // logger.debug('flightHistory: ' + JSON.stringify(flightHistory));
                return this.processFlightsHistory(flightHistory);
                // return flightHistory;
            },
            (error) => {
                throw new InternalServerErrorException(error);
            },
        );

    }

    /**
     * Create new flight
     *
     * @returns {Promise<any>}
     * @memberof FlightChain2Service
     */
    public createFlight(flight: AcrisFlight): Promise<FlightChainData> {
        // assume for flight chain that private data is in "extensions" element
        const transientMap = {};
        if (flight.extensions) {
            for (const prop in flight.extensions) {
                if (flight.extensions.hasOwnProperty(prop)) {
                    transientMap[prop] = Buffer.from(JSON.stringify(flight.extensions[prop]));
                    flight.extensions[prop] = null;
                }
            }
        }

        return this.requestHelper.invokeRequest(FlightChainMethod.createFlight, [JSON.stringify(flight)], transientMap)
            .then(
            (response) => {
                return this.getFlight(ACRISFlightValidation.generateUniqueKey(flight));
            }, (error) => {
                throw new InternalServerErrorException(error);
            });
    }

    /**
     * Update an existing flight.
     *
     * @returns {Promise<any>}
     * @memberof FlightChain2Service
     */
    public updateFlight(flightKey: string, flightDelta: AcrisFlight): Promise<FlightChainData> {
        // assume for flight chain that private data is in "extensions" element
        const transientMap = {};
        if (flightDelta.extensions) {
            for (const prop in flightDelta.extensions) {
                if (flightDelta.extensions.hasOwnProperty(prop)) {
                    transientMap[prop] = Buffer.from(JSON.stringify(flightDelta.extensions[prop]));
                    flightDelta.extensions[prop] = null;
                }
            }
        }

        return this.requestHelper.invokeRequest(FlightChainMethod.updateFlight,
            [flightKey, JSON.stringify(flightDelta)], transientMap)
            .then(
            (response) => {
                return this.getFlight(flightKey);
            }, (error) => {
                throw new InternalServerErrorException(error);
            });
    }

    /**
     * Get the transaction details for the given transaction id.
     *
     * @param channelName - name of the channel to execute this command on
     * @param transactionId
     */
    public async getTransactionInfo(transactionId: string): Promise<FlightChainData> {
        // logger.debug(`FlightChainService.getTransactionInfo('${channelName}', '${transactionId}')`);
        // await this.verifyConnectedToNetwork();

        return this.requestHelper.queryTransaction(transactionId).then(
            (response) => {
                return response;
            }, (error) => {
                Log.hlf.error('error getting transaction id', error);
                if (error.message.indexOf('Entry not found in index') >= 0) {
                    throw new HttpException(error.message, HttpStatus.NOT_FOUND);
                } else {
                    throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            });
    }

    /**
     * Flight at index 0 is the oldest flight.
     *
     * Each element in the flight history contains the full merged ACRIS flight status. For display purposes
     * we want to show the original ACRIS flight data sent to blockchain, and then the deltas after that.
     * Process the flight updates to identify the delta from two ACRIS array elements.
     *
     * @param flightsHistory
     */
    private processFlightsHistory(flightsHistory: IFlightChainHistory[]): FlightChainData[] {
        flightsHistory.forEach( (flight) => Log.hlf.debug(flight.flightData));

        const deltas = [];

        /**
         * Add the creation element to the start of the array.
         */
        const creationFlightData = _.clone(flightsHistory[0]) as IFlightChainHistory;
        creationFlightData.flightData.timestamp =
            FlightChain2Service.dateFromBlockTimestamp(creationFlightData.timestamp);

        const currentFlightData = this.deepClone(flightsHistory[flightsHistory.length - 1]) as IFlightChainHistory;
        currentFlightData.flightData.timestamp =
            FlightChain2Service.dateFromBlockTimestamp(currentFlightData.timestamp);
        deltas.push(currentFlightData.flightData);

        /**
         * Now process all other flights to just add the deltas to this array
         */
        for (let i = flightsHistory.length - 1; i > 0; i--) {

            const original: IFlightChainHistory = _.clone(flightsHistory[i - 1]) as IFlightChainHistory;
            const merged: IFlightChainHistory = _.clone(flightsHistory[i]) as IFlightChainHistory;

            const deepDiff: FlightChainData =
                this.difference(merged.flightData, original.flightData) as FlightChainData;
            const mergedCopy = _.clone(merged);
            mergedCopy.flightData = deepDiff;
            mergedCopy.flightData.timestamp = FlightChain2Service.dateFromBlockTimestamp(flightsHistory[i].timestamp);
            if (!mergedCopy.flightData.updaterId) {
                mergedCopy.flightData.updaterId = flightsHistory[i].flightData.updaterId;
            }

            deltas.push(mergedCopy.flightData);
        }
        deltas.push(creationFlightData.flightData);

        // deltas.forEach( flight => Log.hlf.debug('DELTAS: ' + JSON.stringify(flight)));

        return deltas;
    }

    /**
     * Deep diff between two object, using lodash
     * @param  {Object} object Object compared
     * @param  {Object} base   Object to compare with
     * @return {Object}        Return a new object who represent the diff
     */
    private difference(object: FlightChainData, base: FlightChainData): any {
        function changes(orig: any, comp: any) {
            return _.transform(orig, (result: any, value: any, key: any) => {
                if (!_.isEqual(value, comp[key])) {
                    result[key] = (_.isObject(value) && _.isObject(comp[key])) ? changes(value, comp[key]) : value;
                }
            });
        }

        return changes(object, base);
    }

    private deepClone(original: IFlightChainHistory): any {
        return JSON.parse(JSON.stringify(original));
    }
}
